### -*-Makefile-*- pour préparer les données ouvertes BIXI uniformes
##
## Copyright (C) 2022 Vincent Goulet
##
## 'make get-bixi-data' télécharge les données BIXI.
##
## 'make unzip-bixi-data' décompresse les données BIXI.
##
## 'make data' produit les données uniformes d'historique des
## déplacements.
##
## 'make stations' produit les données uniformes d'état des stations.
##
## 'make zip' produit les archives de données uniformes.
##
## 'make release' téléverse les archives dans le registre de
## paquetages dans GitLab, met à jour la page web et crée une
## étiquette pour la version du code source.
##
## 'make all' est équivalent à 'make zip' pour éviter les publications
## accidentelles.
##
## La configuration s'effectue dans le fichier Makeconf.
##
## Auteur: Vincent Goulet
##
## Ce fichier fait partie du projet "Données ouvertes BIXI uniformes"
## https://gitlab.com/vigou3/bixi-donnes-ouvertes-uniformes

## Configuration dans le fichier Makeconf.
include ./Makeconf

## Répertoire contenant les données originales de BIXI.
SRCDIR := bixi

## Dépôt GitLab et authentification
REPOSNAME = bixi-donnees-ouvertes-uniformes
APIURL = https://gitlab.com/api/v4/projects/vigou3%2F${REPOSNAME}
OAUTHTOKEN = $(shell cat ~/.gitlab/token)

## Titres des colonnes; toujours pris dans les données de l'année de
## référence.
HEADER_DATA := $(shell head -n1 ${SRCDIR}/${SOURCE_${REFYEAR}})
HEADER_STATIONS := $(shell head -n1 ${SRCDIR}/${STATIONS_${REFYEAR}})

## Données à générer: les mois d'avril à novembre de chaque année
## (sauf novembre 2019) pour les données d'historique des
## déplacements; les données d'état des stations pour chaque année.
DATA := $(foreach y, ${YEAR}, \
	          $(addsuffix ${SUFFIX},$(addprefix ${DATAPREFIX}${y}-,${MONTH})))
DATA := $(filter-out ${DATAPREFIX}2019-11${SUFFIX},${DATA}) # retirer novembre 2019
STATIONS := $(addsuffix ${SUFFIX},$(addprefix ${STATIONSPREFIX},${YEAR}))

## Archives à créer -- une variable par archive (utile plus loin) et
## une variable qui les contient toutes.
##
## Le numéro SHA des fichiers BIXI est conservé pour identifier les
## données.
extract_sha = $(shell echo ${BIXI_$(1)} | grep -o -e '-[^-]*\.zip')
$(foreach y,${YEAR},$(eval ZIPFILE_${y} = ${y}-${ZIPSTEM}$(call extract_sha,${y})))
ZIPFILE = $(foreach y,${YEAR},${ZIPFILE_${y}})

## Outils
ZIP := zip --filesync -j9X
UNZIP := unzip -j -d ${SRCDIR}
CURL := curl -O -L


all: zip

## Règles pour créer les fichiers d'état des stations: une règle
## générique et une règle spécifique pour chaque année (créées
## dynamiquement).
${STATIONSPREFIX}%${SUFFIX}:
	@printf "création de %s... " $@
	@{ echo "${HEADER_STATIONS}"; tail -n +2 $<; } > $@
	@printf "%s\n" "ok"

define stations_template
${STATIONSPREFIX}$(1)${SUFFIX}: ${SRCDIR}/${STATIONS_$(1)}
endef
$(foreach y,${YEAR},$(eval $(call stations_template,${y})))

## Règles pour créer les fichiers de données d'historique des
## déplacements: une règle générique et une règle spécifique pour
## chaque année et chaque mois (créées dynamiquement).
##
## La règle générique traite séparément le cas où il y a un seul
## fichier de données BIXI pour toute l'année de celui ou il y a des
## fichiers mensuels.
${DATAPREFIX}%${SUFFIX}:
	@printf "création de %s... " $@
	@{ \
	    if [ "$(words $^)" -eq "1" ]; then \
	        awk -v year=$(shell echo $@ | sed 's/${DATAPREFIX}//' | cut -c 1-4) \
	            'BEGIN { print "${HEADER_DATA}" } \
	             match($$1, "^$*-")' $< > $@; \
	    else \
	        { echo "${HEADER_DATA}"; \
	          tail -n +2 $(filter %$*.csv,$^); } > $@; \
	    fi \
	}
	@printf "%s\n" "ok"

define data_template
$(foreach m,${MONTH},$(eval ${DATAPREFIX}$(1)-${m}${SUFFIX}: $(addprefix ${SRCDIR}/,${SOURCE_$(1)})))
endef
$(foreach y,${YEAR},$(eval $(call data_template,${y})))

## Règles pour la création des archives: une règle générique et une
## règle spécifique pour chaque année (créées dynamiquement).
%.zip:
	@printf "archivage dans %s...\n" $@
	@${ZIP} $@ $?
	@printf "%s\n" "ok"

define zip_template
${ZIPFILE_$(1)}: $(filter ${DATAPREFIX}$(1)-%,${DATA}) \
	     $(filter ${STATIONSPREFIX}$(1)%,${STATIONS})
endef
$(foreach y,${YEAR},$(eval $(call zip_template,${y})))

## Interfaces.
.PHONY: get-bixi-data
get-bixi-data:
	@for f in ${BIXI_DATA}; do \
	    ${CURL} "https://sitewebbixi.s3.amazonaws.com/uploads/docs/$${f}"; \
	done

.PHONY: unzip-bixi-data
unzip-bixi-data: ${BIXI_DATA}
	@for f in ${BIXI_DATA}; do \
	    ${UNZIP} $${f}; \
	done

.PHONY: data
data: ${DATA}

.PHONY: stations
stations: ${STATIONS}

.PHONY: zip
zip: ${ZIPFILE}

.PHONY: release
release: create-link check-status create-tag publish

.PHONY: create-link
create-link: ${ZIPFILE}
	@printf "vérification du contenu du registre... "
	$(eval pkg_id=$(shell curl --header "PRIVATE-TOKEN: ${OAUTHTOKEN}" \
	                           --silent \
	                           "${APIURL}/packages" \
	                      | grep -o -E '\{.*"version":"${VERSION}"[^}]*}' \
	                      | grep -o '"id":[0-9]*' | cut -d: -f 2))
	$(eval files=$(shell curl --header "PRIVATE-TOKEN: ${OAUTHTOKEN}" \
	                         --silent \
	                         "${APIURL}/packages/${pkg_id}/package_files" \
	                     | grep -o '"file_name":"[^"]*"' | cut -d '"' -f 4))
	@printf "%s\n" "ok"
	@printf "création de la liste des archives à téléverser... "
	$(eval upload=$(filter-out ${files}, $?))
	@printf "%s\n" "ok"
	@{ \
	    if [ -z "${upload}" ]; \
	    then \
	        printf "aucune nouvelle archive à téléverser\n"; \
	    else \
	        printf "téléversement des archives vers le registre\n"; \
	        for f in $(filter-out ${files}, $?); do \
	            printf "  $$f... "; \
	            curl --upload-file "$$f" \
	                 --header "PRIVATE-TOKEN: ${OAUTHTOKEN}" \
	                 --silent \
	                 "${APIURL}/packages/generic/${ZIPSTEM}/${VERSION}/$$f" > /dev/null; \
	            printf "%s\n" "ok"; \
	        done; \
	    fi \
	}

.PHONY: check-status
check-status:
	@{ \
	    printf "%s" "vérification de l'état du dépôt local... "; \
	    branch=$$(git branch --list | grep ^* | cut -d " " -f 2-); \
	    if [ "$${branch}" != "main" ]; \
	    then \
	        printf "\n%s\n" "! pas sur la branche main"; exit 2; \
	    fi; \
	    if [ -n "$$(git status --porcelain | grep -v '^??')" ]; \
	    then \
	        printf "\n%s\n" "! changements non archivés dans le dépôt"; exit 2; \
	    fi; \
	    if [ -n "$$(git log origin/main..HEAD | head -n1)" ]; \
	    then \
	        printf "\n%s\n" "changements non publiés dans le dépôt; publication dans origin"; \
	        git push; \
	    else \
	        printf "%s\n" "ok"; \
	    fi; \
	}

.PHONY: create-tag
create-tag:
	@printf "étiquettage de la version %s...\n" ${VERSION};
	curl --request POST \
	      --header "PRIVATE-TOKEN: ${OAUTHTOKEN}" \
	      --output /dev/null --silent \
	      "${APIURL}/repository/tags?tag_name=${TAGNAME}&ref=main"
	@printf "ok\n"

.PHONY: publish
publish:
	@printf "mise à jour de la page web...\n"
	git checkout pages && \
	  ${MAKE} && \
	  git checkout main
	@printf "ok\n"
