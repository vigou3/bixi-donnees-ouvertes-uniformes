# Données ouvertes BIXI uniformes

[BIXI Montréal](https://www.bixi.com/fr) est l'organisme à but non
lucratif créé en 2014 par la Ville de Montréal pour gérer le système
de vélopartage à Montréal.

Chaque année, l'organisme publie les [données
ouvertes](https://bixi.com/fr/donnees-ouvertes) d'utilisation de son
service. Cependant, le format de ces données change au fil du temps.
Cela complique inutilement la conception d'outils logiciels exploitant
ces données qui demeurent valides quelque soit l'année utilisée.

Le présent projet fournit les données ouvertes originales de BIXI dans
un format uniforme d'une année à l'autre. Les données elles-mêmes
demeurent inchangées.

## Auteur

Vincent Goulet, professeur titulaire, École d'actuariat, Université Laval

## Licence

Le code informatique servant à créer les données est mis à disposition
sous licence [GNU GPL](https://www.gnu.org/licenses/gpl-3.0.fr.html).
Consulter le fichier LICENSE pour la licence complète.

Les données ouvertes BIXI uniformes sont adaptées des [données
ouvertes](https://bixi.com/fr/donnees-ouvertes) de BIXI Montréal. Les
deux jeux de données sont mis à disposition sous licence [Creative
Commons BY-SA
4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.fr).

